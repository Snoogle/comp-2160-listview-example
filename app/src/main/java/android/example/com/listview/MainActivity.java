package android.example.com.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView list = (ListView) findViewById(R.id.theList);

        ArrayList<String> names = new ArrayList<>();
        names.add("Jacky Pham");
        names.add("William Chung");
        names.add("Thomas Winter");
        names.add("Daphne Lun");
        names.add("Roman Iefimov");
        names.add("Sunny Leung");

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, names);
        list.setAdapter(adapter);
    }
}
